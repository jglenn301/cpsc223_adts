#ifndef __CITY_LIST_H__
#define __CITY_LIST_H__

#include <stdbool.h>

#include "cities.h"

// Don't get used to seeing the struct definition here!
typedef struct _city_list
{
  city *elements;
  size_t size;
  size_t capacity;
} city_list;

/**
 * Creates an empty list of cities.
 */
city_list city_list_create();

/**
 * Returns the size of the given list.
 *
 * @param l a pointer to a list of cities
 */
size_t city_list_size(const city_list *l);

/**
 * Returns the city at the given location in this list.
 *
 * @param l a pointer to a list of cities
 * @param i an index into that list
 * @return the city at that position
 */
city city_list_get(const city_list *l, size_t i);

/**
 * Adds the given city to the end of the given list.
 *
 * @param l a pointer to a list of cities
 * @param c a pointer to a city
 * @return true if and only if the city was sucessfully added
 */
bool city_list_add(city_list *l, const city *c);

/**
 * Destroys the given list.
 *
 * @param l a pointer to a list of cities
 */
void city_list_destroy(city_list *l);


#endif

CC=gcc
CFLAGS=-Wall -pedantic -std=c99 -g3

TSP: location.o tsp.o cities.o city_list.o
	${CC} ${CCFLAGS} -o $@ $^ -lm

city_list.o: city_list.h
tsp.o: location.h cities.h city_list.h
location.o: location.h
cities.o: location.h cities.h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>

#include "location.h"
#include "cities.h"
#include "city_list.h"

typedef double distance_t;

#define ERROR (stderr)

// see http://faculty.washington.edu/jtenenbg/courses/342/f08/sessions/tsp.html
// for descriptions of methods

void print_usage(const char *command);	  
city_list read_input(FILE *in);

void route_nearest(int n, int tour[], distance_t **dist);
void route_insert(int n, int tour[], distance_t **dist);
void normalize(int n, int tour[], const city_list *itin);
void swap(int arr[], int i, int j);

double calculate_total(int n, int tour[], distance_t **dist);

int main(int argc, char **argv)
{
  initialize_city_database();
  // TODO: modify to find basename of executable
  
  // open file
  if (argc < 3)
    {
      // missing arguments
      print_usage(argv[0]);
      return 1;
    }

  FILE *infile = fopen(argv[2], "r");
  if (!infile)
    {
      fprintf(ERROR, "%s: could not open %s\n", argv[0], argv[1]);
      return 1;
    }
  
  city_list itin = read_input(infile);
  fclose(infile);

  size_t num_locations = city_list_size(&itin);
  if (num_locations < 2)
    {
      fprintf(ERROR, "%s: must have at least 2 cities\n", argv[0]);
      city_list_destroy(&itin);
      return 1;
    }
  
  distance_t **table = malloc(sizeof(distance_t *) * num_locations);
  for (size_t r = 0; r < num_locations; r++)
    {
      table[r] = malloc(sizeof(distance_t) * num_locations);
      for (size_t c = 0; c < num_locations; c++)
	{
	  city from = city_list_get(&itin, r);
	  city to = city_list_get(&itin, c);
	  table[r][c] = location_distance(&from.coord, &to.coord);
	}
    }
  
  // our tours are referred to by indices into the arrays, so
  // 0 means "the first city in the input"
  int tour[num_locations];
  for (int i = 0; i < num_locations; i++)
    {
      tour[i] = i;
    }
  
  if (strcmp(argv[1], "-given") == 0)
    {
    }
  else if (strcmp(argv[1], "-nearest") == 0)
    {
      route_nearest(num_locations, tour, table);
    }
  else if (strcmp(argv[1], "-insert") == 0)
    {
      route_insert(num_locations, tour, table);
      normalize(num_locations, tour, &itin);
    }
  else
    {
      print_usage(argv[0]);
      return 1;
    }
  
  double total = calculate_total(num_locations, tour, table);
  printf("%8s:%10.2f", argv[1], total);
  for (int i = 0; i < num_locations; i++)
    {
      printf(" %s", city_list_get(&itin, tour[i]).name);
    }
  printf(" %s\n", city_list_get(&itin, tour[0]).name);

  // names are dynamically allocated in the itinerary, so free them...
  for (size_t i = 0; i < num_locations; i++)
    {
      city c = city_list_get(&itin, i);
      free(c.name);
    }

  // ...and then destroy the list
  city_list_destroy(&itin);
  
  // and destroy the 2-D distance table
  for (size_t r = 0; r < num_locations; r++)
    {
      free(table[r]);
    }
  free(table);

  return 0;
}

void print_usage(const char *command)
{
  fprintf(stderr, "USAGE: %s [-given | -nearest | -insert] input-file\n", command);
}

city_list read_input(FILE *in)
{
  city_list input = city_list_create();
  
  char code[4];
  while (fscanf(in, "%3s", code) == 1)
    {
      code[3] = '\0';
      city c;
      c.name = malloc(sizeof(char) * 4);
      strcpy(c.name, code);

      if (find_city(c.name, &c.coord))
	{
	  city_list_add(&input, &c);
	}
    }
  
  return input;
}


void route_nearest(int n, int tour[], distance_t **dist)
{
  // your code here
}

void route_insert(int n, int tour[], distance_t **dist)
{
  // your code here
}

double calculate_total(int n, int tour[], distance_t **dist)
{
  double total = 0.0; // initialize to last to first
  for (int i = 1; i < n; i++)
    {
      total += dist[tour[i - 1]][tour[i]]; // add in prev to curr
    }
  return total + dist[tour[n - 1]][tour[0]];
}

void swap(int arr[], int i, int j)
{
  int temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

void normalize(int n, int tour[], const city_list *itin)
{
  // your code here
}

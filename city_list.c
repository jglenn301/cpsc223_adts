#include <stdlib.h>
#include <stdbool.h>

#include "city_list.h"
#include "cities.h"

#define CITY_LIST_INITIAL_CAPACITY 2

city_list city_list_create()
{
  city_list result;
  result.elements = malloc(sizeof(city) * CITY_LIST_INITIAL_CAPACITY);
  result.size = 0;
  result.capacity = result.elements != NULL ? CITY_LIST_INITIAL_CAPACITY : 0;

  return result;
}

size_t city_list_size(const city_list *l)
{
  if (l == NULL)
    {
      return 0;
    }
  else
    {
      return l->size;
    }
}

city city_list_get(const city_list *l, size_t i)
{
  if (l == NULL || i < 0 || i >= l->size)
    {
      city nowhere = {"", {0.0, 0.0}};
      return nowhere;
    }
  
  return l->elements[i];
}

bool city_list_add(city_list *l, const city *c)
{
  if (l == NULL || c == NULL)
    {
      return false;
    }

  if (l->capacity > 0 && l->size == l->capacity)
    {
      city *bigger = realloc(l->elements, sizeof(city) * l->capacity * 2);
      if (bigger != NULL)
	{
	  l->elements = bigger;
	  l->capacity *= 2;
	}
    }
  
  if (l->size < l->capacity)
    {
      l->elements[l->size] = *c;
      l->size++;
      return true;
    }
  else
    {
      return false;
    }
}

void city_list_destroy(city_list *l)
{
  free(l->elements);
  l->elements = NULL;
  l->size = 0;
  l->capacity = 0;
}
